import { Component, OnInit } from '@angular/core';
import { SoapTempConverterService } from 'src/app/services/soap-temp-converter.service';

@Component({
  selector: 'app-temp-converter',
  templateUrl: './temp-converter.component.html',
  styleUrls: ['./temp-converter.component.scss']
})
export class TempConverterComponent implements OnInit {

  farenheit: number = 0;
  constructor(private soapTempConverterService: SoapTempConverterService) { }

  ngOnInit(): void {
  }

  convertToFarenheit(event: any) {
    console.log(event.target.value)
    let centigrados = event.target.value;
    if (centigrados) {
      this.soapTempConverterService.convertCelsiusToFahrenheit(centigrados).subscribe((farenheit: number) => {
        this.farenheit = farenheit;
      }
      )
    }

  }
}
