
import { Component, OnInit } from '@angular/core';
import { catchError } from 'rxjs';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { SoapTempConverterService } from 'src/app/services/soap-temp-converter.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  centigrados:number=0;

  constructor(private soapTempConverterService:SoapTempConverterService) { }

  ngOnInit(): void {

  }




  convertToFarenheit(centigrados:number){
    return this.soapTempConverterService.convertCelsiusToFahrenheit(centigrados);
  }
        
        
     
}
