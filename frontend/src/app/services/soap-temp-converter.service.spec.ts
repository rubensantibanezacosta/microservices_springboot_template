import { TestBed } from '@angular/core/testing';

import { SoapTempConverterService } from './soap-temp-converter.service';

describe('SoapTempConverterService', () => {
  let service: SoapTempConverterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SoapTempConverterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
