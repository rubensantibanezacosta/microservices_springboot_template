import { HttpClient, HttpErrorResponse,  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { Product } from '../models/product';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient:HttpClient) { }


  getProducts():Observable<Product[]>{
    return this.httpClient.get<Product[]>(environment.apiGatewayUrl+'/api/product').pipe(
      catchError(this.handleError)
    );
  }

  getProduct(id:number):Observable<Product>{
    return this.httpClient.get<Product>(environment.apiGatewayUrl+'/api/product/'+id).pipe(
      catchError(this.handleError)
    );
  }

  createProduct(product:Product):Observable<Product>{
    return this.httpClient.post<Product>(environment.apiGatewayUrl+'/api/product', JSON.stringify(product)).pipe(
      catchError(this.handleError)
    );
  }

  updateProduct(product:Product):Observable<Product>{
    return this.httpClient.put<Product>(environment.apiGatewayUrl+'/api/product', JSON.stringify(product)).pipe(
      catchError(this.handleError)
    );
  }

  deleteProduct(id:number):Observable<Product>{
    return this.httpClient.delete<Product>(environment.apiGatewayUrl+'/api/product/'+id).pipe(
      catchError(this.handleError)
    );
  }


  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }
}
