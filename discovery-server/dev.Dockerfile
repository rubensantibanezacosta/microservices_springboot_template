
#compile
FROM maven:3.8.4-openjdk-17


RUN echo src >> .dockerignore
COPY . .


RUN mvn dependency:resolve

RUN rm -rf .dockerignore

COPY src/ /src/



RUN mvn clean -DskipTests=true

RUN mvn install -DskipTests=true 

CMD ["mvn","spring-boot:run"]
