package com.rubendev.soap.controllers;

import com.rubendev.soap.services.CelsiusToFarenheitImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class TemperaturesController {

    public CelsiusToFarenheitImpl celsiusToFarenheit;

    @Autowired
    public TemperaturesController(CelsiusToFarenheitImpl celsiusToFarenheit) {
        this.celsiusToFarenheit = celsiusToFarenheit;
    }

    @GetMapping("/api/celsiustofarenheit/{celsius}")
    public ResponseEntity<?> celsiusToFarenheit(@PathVariable("celsius") String celsius) {
       try{
           double farenheit = celsiusToFarenheit.convertCelsiusToFahrenheit(Double.parseDouble(celsius));
           return new ResponseEntity<>(farenheit, HttpStatus.OK);
       } catch (Exception e) {
           e.printStackTrace();
              return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }
}
