package com.rubendev.soap.services;


import com.learnwebservices.services.tempconverter.CelsiusToFahrenheitRequest;
import com.learnwebservices.services.tempconverter.TempConverterEndpoint;
import com.learnwebservices.services.tempconverter.TempConverterEndpointService;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@Component
public class CelsiusToFarenheitImpl implements CelsiusToFarenheit {




    public CelsiusToFarenheitImpl() {
    }

    @Override
    public double convertCelsiusToFahrenheit(double celsius) throws MalformedURLException {
        TempConverterEndpointService ss = new TempConverterEndpointService(new URL("file:"+System.getProperty("user.dir")+"src/main/resources/wsdl/service.wsdl"));
        TempConverterEndpoint port = ss.getTempConverterEndpointPort();
        CelsiusToFahrenheitRequest request = new CelsiusToFahrenheitRequest();
        request.setTemperatureInCelsius(celsius);
        return port.celsiusToFahrenheit(request).getTemperatureInFahrenheit();
    }

}
