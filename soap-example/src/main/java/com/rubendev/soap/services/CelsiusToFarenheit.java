package com.rubendev.soap.services;

import java.net.MalformedURLException;

public interface CelsiusToFarenheit {
    public double convertCelsiusToFahrenheit(double celsius) throws MalformedURLException;
}
