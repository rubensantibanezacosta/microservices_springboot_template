#compile
FROM maven:3.8.4-openjdk-17 as build-step

COPY . .


RUN mvn clean -DskipTests=true

RUN mvn install -DskipTests=true


#run

FROM openjdk:17

COPY --from=build-step ./target/config-service.jar config-service.jar
# COPY --chown=101:101 healthz.html .
ENTRYPOINT [ "java","-jar","/config-service.jar" ]
