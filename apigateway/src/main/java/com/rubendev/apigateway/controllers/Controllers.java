package com.rubendev.apigateway.controllers;

import org.slf4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class Controllers {
    private final Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());

}
