package com.rubendev.apigateway.dtos;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Producto {

    private Long id;

    private String name;

    private String description;

    private Double price;

}
