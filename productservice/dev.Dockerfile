
#compile
FROM maven:3.8.4-openjdk-17



COPY pom.xml pom.xml


RUN mvn dependency:resolve

COPY . .



RUN mvn clean -DskipTests=true

RUN mvn install -DskipTests=true 

CMD ["mvn","spring-boot:run"]
