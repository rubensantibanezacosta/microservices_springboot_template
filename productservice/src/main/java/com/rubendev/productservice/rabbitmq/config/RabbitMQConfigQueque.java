package com.rubendev.productservice.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


public class RabbitMQConfigQueque {

//
//    public static final String ROUTING_A="routing.A";
//    public static final String ROUTING_B="routing.B";
//
//    public static final String ROUTING_ALL="routing.*";
//
//    public static final String QUEUE_A="queue.A";
//    public static final String QUEUE_B="queue.B";
//
//    public static final String QUEUE_ALL="queue.all";
//
//    private static final boolean IS_DURABLE_QUEUE = true;
//
//    @Bean(name = "queue.A")
//
//    Queue queueA() {
//        return new Queue(QUEUE_A, IS_DURABLE_QUEUE);
//    }
//
//    @Bean(name = "queue.B")
//    Queue queueB() {
//        return new Queue(QUEUE_B, IS_DURABLE_QUEUE);
//    }
//
//    @Bean(name = "queue.all")
//    Queue queueAll() {
//        return new Queue(QUEUE_ALL, IS_DURABLE_QUEUE);
//    }
//
//    @Bean
//    TopicExchange exchange() {
//        return new TopicExchange("exchange.topic");
//    }
//
//    @Bean
//
//    Binding bindingExchangeA(@Qualifier("queue.A") Queue queueA, TopicExchange exchange) {
//        return BindingBuilder.bind(queueA).to(exchange).with(ROUTING_A);
//    }
//
//    @Bean
//    Binding bindingExchangeB(@Qualifier("queue.B") Queue queueB, TopicExchange exchange) {
//        return BindingBuilder.bind(queueB).to(exchange).with(ROUTING_B);
//    }
//
//    @Bean
//    Binding bindingExchangeAll(@Qualifier("queue.all") Queue queueAll, TopicExchange exchange) {
//        return BindingBuilder.bind(queueAll).to(exchange).with(ROUTING_ALL);
//    }
//
//
//
//
//    @Bean
//    MessageConverter messageConverter(){
//        return new Jackson2JsonMessageConverter();
//    }
//
//    @Bean
//    RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
//        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
//        rabbitTemplate.setMessageConverter(messageConverter());
//        return rabbitTemplate;
//    }
}
