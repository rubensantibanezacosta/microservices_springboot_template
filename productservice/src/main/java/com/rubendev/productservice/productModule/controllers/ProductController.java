package com.rubendev.productservice.productModule.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rubendev.productservice.productModule.entities.Product;
import com.rubendev.productservice.productModule.services.ProductImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProductController {

    @Autowired private ProductImpl productService;

    @GetMapping("/api/product")
    public ResponseEntity<?> getAllProducts() {
        try{
            return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value="/api/product", consumes="application/json")
    public ResponseEntity<?> createProduct(@RequestBody String jsonProduct) {
        try{
            ObjectMapper mapper = new ObjectMapper();
            Product product = mapper.readValue(jsonProduct, Product.class);
            if(product.getDescription().isEmpty() || product.getName().isEmpty() || product.getPrice() == 0) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(productService.createProduct(product), HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value="/api/product", consumes="application/json")
    public ResponseEntity<?> updateProduct(@RequestBody String jsonProduct) {
        try{
            ObjectMapper mapper = new ObjectMapper();
            Product product = mapper.readValue(jsonProduct, Product.class);
            if(product.getDescription().isEmpty() || product.getName().isEmpty() || product.getPrice() == 0 || product.getId() == null) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(productService.updateProduct(product), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/api/product/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Long id) {
        try{
            return new ResponseEntity<>(productService.deleteProduct(id), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
