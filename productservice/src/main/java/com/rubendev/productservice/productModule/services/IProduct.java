package com.rubendev.productservice.productModule.services;

import com.rubendev.productservice.productModule.entities.Product;

import java.util.List;
import java.util.Optional;

public interface IProduct {
    public Optional<Product> getProduct(Long id);
    public List<Product> getAllProducts();
    public Product createProduct(Product product);
    public int updateProduct(Product product);
    public int deleteProduct(Long id);
}
