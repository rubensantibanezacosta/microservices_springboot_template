package com.rubendev.templateRabbit.config;

import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfigQueque {

    public static final String ROUTING_A="routing.A";
    public static final String ROUTING_B="routing.B";

    public static final String QUEUE_A="queque.A";
    public static final String QUEUE_B="queque.B";
    private static final boolean IS_DURABLE_QUEUE = true;

    @Bean
    Queue queueA() {
        return new Queue(RabbitMQConfigQueque.QUEUE_A, RabbitMQConfigQueque.IS_DURABLE_QUEUE);
    }

    @Bean
    Queue queueB() {
        return new Queue(RabbitMQConfigQueque.QUEUE_B, RabbitMQConfigQueque.IS_DURABLE_QUEUE);
    }

//    @Bean
//    TopicExchange topicExchange() {
//        return new TopicExchange(EXCHANGE_NAME);
//    }
    @Bean
    FanoutExchange directExchange() {
        return new FanoutExchange("exchange.fanout");
    }

    @Bean
    Binding bindingExchangeA(Queue queueA, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queueA).to(fanoutExchange)/*.with(RabbitMQConfigQueque.ROUTING_A)*/;
    }

    @Bean
    Binding bindingExchangeB(Queue queueB, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queueB).to(fanoutExchange)/*.with(RabbitMQConfigQueque.ROUTING_B)*/;
    }


    @Bean
    MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }
}
