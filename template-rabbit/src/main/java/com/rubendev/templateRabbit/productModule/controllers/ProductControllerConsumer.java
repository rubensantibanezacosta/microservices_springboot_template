package com.rubendev.templateRabbit.productModule.controllers;

import com.rubendev.templateRabbit.productModule.entities.Product;
import org.slf4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class ProductControllerConsumer {

    private final Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());
    @RabbitListener(queues = "queque.A")
    private void  receiveMessageFromA(Product product) {
        logger.info("Received from quequeA->{}", product);
    }

    @RabbitListener(queues = "queque.B")
    private void  receiveMessageFromB(Product product) {
        logger.info("Received  from quequeB->{}", product);
    }
}
