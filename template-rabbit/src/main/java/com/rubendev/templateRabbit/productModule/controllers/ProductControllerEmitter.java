package com.rubendev.templateRabbit.productModule.controllers;

import com.rubendev.templateRabbit.productModule.entities.Product;
import com.rubendev.templateRabbit.productModule.services.ProductImpl;
import org.slf4j.Logger;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

@RestController
public class ProductControllerEmitter {

    private final Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Autowired
    TopicExchange topicExchange;

    @Autowired
    ProductImpl productService;



@PostMapping(value="/api/productsa", consumes = "application/json")
    public String sendToA(@RequestBody Product product) {
        logger.info("Sending to quequeA->{}", product);
        rabbitTemplate.convertAndSend(topicExchange.getName(),"queque.A", product);
        return "Producto enviado correctamente a A";
}

    @PostMapping(value="/api/productsb", consumes = "application/json")
    public String sendToB(@RequestBody Product product) {
        logger.info("Enviando a B->{}", product);
        rabbitTemplate.convertAndSend(topicExchange.getName(),"queque.B", product);
        return "Producto enviado correctamente a B";
    }

//    @GetMapping("/api/products/{id}")
//    public ResponseEntity<?> getProductoById(@PathVariable Long id) {
//        try{
//            return new ResponseEntity<>(productService.getProduct(id), org.springframework.http.HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(e.getMessage(), org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

//    @GetMapping("/api/products")
//    public ResponseEntity<?> getAllProducts() {
//        try{
//            return new ResponseEntity<>(productService.getAllProducts(), org.springframework.http.HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(e.getMessage(), org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//
//    @PostMapping(value="/api/products", consumes = "application/json")
//    public ResponseEntity<?> createProduct(@RequestBody String product) {
//
//        try{
//            ObjectMapper mapper = new ObjectMapper();
//            Product producto = mapper.readValue(product, Product.class);
//
//            if(producto.getName().isEmpty() || producto.getDescription().isEmpty() || producto.getPrice() == null) {
//                return new ResponseEntity<>("El producto no puede estar vacio", org.springframework.http.HttpStatus.BAD_REQUEST);
//            }
//
//            return new ResponseEntity<>(productService.createProduct(producto), HttpStatus.CREATED);
//        } catch (Exception e) {
//            return new ResponseEntity<>(e.getMessage(), org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//
//    @PutMapping(value="/api/products/{id}", consumes = "application/json")
//    public ResponseEntity<?> updateProduct(@PathVariable Long id,@RequestBody  String product) {
//        try{
//            ObjectMapper mapper = new ObjectMapper();
//            Product producto = mapper.readValue(product, Product.class);
//
//            if(producto.getName().isEmpty() || producto.getDescription().isEmpty() || producto.getPrice() == null) {
//                return new ResponseEntity<>("El producto no puede estar vacio", org.springframework.http.HttpStatus.BAD_REQUEST);
//            }
//
//            return new ResponseEntity<>(productService.updateProduct(producto), org.springframework.http.HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(e.getMessage(), org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//
//    @DeleteMapping("/api/products/{id}")
//    public ResponseEntity<?> deleteProduct(@PathVariable Long id) {
//        try{
//            return new ResponseEntity<>(productService.deleteProduct(id), org.springframework.http.HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(e.getMessage(), org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
}
