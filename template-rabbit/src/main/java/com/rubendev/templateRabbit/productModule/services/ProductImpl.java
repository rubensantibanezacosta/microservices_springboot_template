package com.rubendev.templateRabbit.productModule.services;


import com.rubendev.templateRabbit.productModule.entities.Product;
import com.rubendev.templateRabbit.productModule.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ProductImpl implements IProduct {

@Autowired private ProductRepository productRepository;
    @Override
    public Optional<Product> getProduct(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public List<Product> getAllProducts() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public int updateProduct(Product product) {
     Optional<Product> savedProduct = productRepository.findById(product.getId());
        if(savedProduct.isPresent()) {
            productRepository.save(product);
            return 1;
        }
        return 0;
    }

    @Override
    public int deleteProduct(Long id) {
        Optional<Product> savedProduct = productRepository.findById(id);
        if(savedProduct.isPresent()) {
            productRepository.deleteById(id);
            return 1;
        }
        return 0;
    }
}
