package com.rubendev.templateRabbit.productModule.services;


import com.rubendev.templateRabbit.productModule.entities.Product;

import java.util.List;
import java.util.Optional;

public interface IProduct {
    public Optional<Product> getProduct(Long id);
    public List<Product> getAllProducts();
    public Product createProduct(Product product);
    public int updateProduct(Product product);
    public int deleteProduct(Long id);
}
