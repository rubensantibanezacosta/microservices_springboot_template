package com.rubendev.templateRabbit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TemplateRabbitApplication {


	public static void main(String[] args) {
		SpringApplication.run(TemplateRabbitApplication.class, args);
	}


}
